'use strict';

  var currencyCloud = function ($http) {

    var queries = {
      login :
        function login() {
          return $http.get('/currencycloud/login')
            .success(function(data, status, headers, config) {
              return status;
            })
            .error(function(data, status, headers, config) {
              console.log(data);
            });
        },
      getQuote :
        function getQuote (quoteOptions) {
          return $http({
            method : 'GET',
            url : '/currencycloud/getquote',
            params : quoteOptions
          })
            .success(function(data, status, headers, config) {
              return status;
            })
            .error(function(data, status, headers, config) {
              console.log('err');
            });
        },
      convert :
        function convert (convertOptions) {
          return $http({
            method : 'GET',
            url : '/currencycloud/convert',
            params : convertOptions
          })
            .success(function(data, status, headers, config) {
              return status;
            })
            .error(function(data, status, headers, config) {
              console.log('err');
            });
        }

    };
    return queries;

  };

  angular.module('interface').factory('currencyCloud', ['$http', currencyCloud]);
