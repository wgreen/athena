'use strict';

// Setting up route
angular.module('users').config(['$stateProvider',
	function($stateProvider) {
		// Users state routing
		$stateProvider.
  		state('invoices', {
  			url: '/invoices',
  			templateUrl: 'modules/interface/views/invoice.html',
        controller : 'interfaceCtrl'
  		});
	}
]);