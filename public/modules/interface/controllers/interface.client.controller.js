
function interfaceCtrl (currencyCloud, $scope) {

  $scope.name = 'me';

  $scope.quoteOptions = {
      buy_currency : 'EUR',
      sell_currency : 'GBP',
      amount : 10000,
      fixed_side : 'buy'
    };

  var convertOptions = {
    buy_currency : 'EUR',
    sell_currency : 'GBP',
    amount : 10000,
    fixed_side : 'buy',
    reason : 'Invoice Payment',
    term_agreement : true
  };

  $scope.login = function () {
    return currencyCloud.login();
  };

  $scope.getQuote = function () {
    return currencyCloud.getQuote($scope.quoteOptions);
  };

  $scope.convert = function () {
    return currencyCloud.convert(convertOptions);
  };


}




angular.module('interface').controller('interfaceCtrl', ['currencyCloud', '$scope', interfaceCtrl]);