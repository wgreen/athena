'use strict';

// currency cloud queries
var curl = require('curlrequest');
var Q = require('q');

/*
DEMO API Key for Currency Cloud:
Login ID: bernard.willer@blacklynx.co.za
API Key: 776b8436987a33161bcb2d310cd1abaa4d9c37b9a861a92c9e2e3d5f30fb4b91
*/

var token;

// Utlity function that takes an object and returns a dataString
// compatible with curl's data value.
var dataString =
function dataString(options) {
  var result;
  var obStr = '';

  for(var prop in options) {
    if(options.hasOwnProperty(prop)) {
      obStr += '&' + prop + '=' + options[prop];
    };
  }

  result = obStr.replace(new RegExp('&'), '');
// e.g. buy_currency=EUR&sell_currency=GBP&amount=10000&fixed_side=buy
  return result;
};


// Authenticate
exports.getAuthToken =
function getAuthToken(req, res) {
  var result;
  curl.request({
      url: 'https://devapi.thecurrencycloud.com/v2/authenticate/api',
      method : 'POST',
      data : 'login_id=bernard.willer@blacklynx.co.za&api_key=776b8436987a33161bcb2d310cd1abaa4d9c37b9a861a92c9e2e3d5f30fb4b91'
    },
    function (err, stdout) {
      if(err) {
        res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      }
      if(stdout) {
        result = JSON.parse(stdout).auth_token;
        req.session.currencyapi = result;
        res.status(200).jsonp(result);
      }
    });
};


// GET A QUOTE
exports.getQuote =
function getQuote(req, res) {
  var token = req.session.currencyapi;
  var datas = req._parsedUrl.query;
  var result;

  curl.request({
    method : 'GET',
    url : 'https://devapi.thecurrencycloud.com/v2/rates/detailed',
    headers : { 'X-Auth-Token' : token },
    data : datas
  },
  function (err, stdout) {
    if(err) {
      res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }
    if(stdout) {
      result = JSON.parse(stdout);
      res.status(200).jsonp(result);
    }
  });
}


// CONVERT
exports.convert =
  function convert(req, res) {
    var token = req.session.currencyapi;
    var datas = req._parsedUrl.query;
    var result;

    curl.request({
      method : 'POST',
      url : 'https://devapi.thecurrencycloud.com/v2/conversions/create',
      headers : { 'X-Auth-Token' : token },
      data :  datas
    },
    function (err, stdout) {
      if(err) {
        res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      }
      if(stdout) {
        result = JSON.parse(stdout);
        console.log(result);
        res.status(200).jsonp(result);
      }
    });
  }


var addBenOptions = {
  currency : 'ZAR',
  bank_account_country : 'ZA',
  account_number : '123456783',
  bic_swift : 'COBADEFF',
  // the following are for the createBeneficiary()
  bank_account_holder_name : 'Tasty Biltong',
  beneficiary_country : 'ZA',
  bank_country : 'ZA',
  iban : 'DE89370400440532013000'
};
/* Requirements for ZA are
{ payment_type: 'priority',
  acct_number: '^\\d{1,16}$',
  bic_swift: '^[0-9A-Z]{8}$|^[0-9A-Z]{11}$' }
*/

// Add a Beneficiary
//
// AT the moment we do not need this - Need to update if needed in future
//
// We want to make a priority payment to a supplier based in Germany - change to south africa.
// To do this, we first need to check which details are required.

exports.addBeneficiary =
  function addBeneficiary (req, res) {
    var token = req.session.currencyapi;
    var datas = req._parsedUrl.query;
    var result;
    var reqs;

    // checkRequired(); // Skipping this as the api does not work - it
    // does not return a complete list of the required values
    createBeneficiary();

    function checkRequired() {

      curl.request({
        method : 'GET',
        url : 'https://devapi.thecurrencycloud.com/v2/reference/beneficiary_required_details',
        headers : { 'X-Auth-Token' : token },
        data :  'currency=' + addBenOptions.currency +
                '&bank_account_country=' + addBenOptions.bank_account_country

      },
      function(err, stdout) {
        if (err) {console.log(err);}
        if (stdout) {
          reqs = JSON.parse(stdout).details[0];
          console.log(reqs);
        }
      });
    } // checkReqDets


    function createBeneficiary () {
      curl.request({
        method : 'POST',
        url : 'https://devapi.thecurrencycloud.com/v2/beneficiaries/create',
        headers : { 'X-Auth-Token' : token },
        data : datas
/*        data :  'currency=' + addBenOptions.currency +
                '&bank_account_holder_name=' + addBenOptions.bank_account_holder_name +
                '&beneficiary_country='  + addBenOptions.beneficiary_country +
                '&bank_country=' + addBenOptions.bank_country +
                '&account_number=' + addBenOptions.account_number +
                '&bic_swift=' + addBenOptions.bic_swift*/
      },
      function (err, stdout) {
        if (err) {console.log(err);}
        if (stdout) {console.log(stdout);}
      });
    }

} // addBeneficiary


var payOptions = {
  currency : 'ZAR',
  conversion_id : '11441095-87a4-4905-a3a0-cdbe312fc02e', // WHERE?
  beneficiary_id : 'ce267c2d-22cd-44b0-8cb0-43d069607e38',
  amount : 10000,
  reason : 'Invoice Payment',
  payment_type : 'priority',
  reference : 'Invoice 1234'
};

// PAY

function pay(token, payOptions) {
  curl.request({
    method : 'POST',
    url : 'https://devapi.thecurrencycloud.com/v2/payments/create',
    headers : { 'X-Auth-Token' : token },
    data :  'currency=' + payOptions.currency +
            '&conversion_id=' + payOptions.conversion_id +
            '&beneficiary_id=' + payOptions.beneficiary_id +
            '&amount=' + payOptions.amount +
            '&reason=' + payOptions.reason +
            '&payment_type=' + payOptions.payment_type +
            '&reference=' + payOptions.reference
  },
  function (err, stdout) {
    if (err) {console.log(err);}
    if (stdout) {console.log(stdout);}
  });
}



// CLOSE SESSION
// close the session when the user logs out
// note it will expire anyway if we dont
// GET A QUOTE
exports.closeSession =
function closeSession(req, res) {
  var token = req.session.currencyapi;
  curl.request({
    method : 'POST',
    url : 'https://devapi.thecurrencycloud.com/v2/authenticate/close_session',
    headers : {'X-Auth-Token' : token}
  },
  function (err, stdout) {
    if(err) {
      res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }
    if(stdout) {
      res.status(200).jsonp(stdout);
    }
  });
}

