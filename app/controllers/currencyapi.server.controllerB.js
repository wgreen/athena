// currency cloud queries
var curl = require('curlrequest');
var Q = require('q');

/*
DEMO API Key for Currency Cloud:
Login ID: bernard.willer@blacklynx.co.za
API Key: 776b8436987a33161bcb2d310cd1abaa4d9c37b9a861a92c9e2e3d5f30fb4b91
*/

var token;


// Authenticate
var getAuthToken =
function getAuthToken () {
  var deferred = Q.defer();
  curl.request({
      url: 'https://devapi.thecurrencycloud.com/v2/authenticate/api',
      method : 'POST',
      data : 'login_id=bernard.willer@blacklynx.co.za&api_key=776b8436987a33161bcb2d310cd1abaa4d9c37b9a861a92c9e2e3d5f30fb4b91'
    },
    function (err, stdout) {
      if(err) {
        deferred.reject(new Error(err));
        console.log(err + err.stack);
      }
      if(stdout) {
        deferred.resolve(JSON.parse(stdout).auth_token);
      }
    });
  return deferred.promise;
};

// e.g. options obj to pass in to getQuote
// for demo purposes
var quoteOptions = {
 buy_currency : 'EUR',
 sell_currency : 'GBP',
 amount : 10000,
 fixed_side : 'buy'
};

var dataString =
function dataString(options) {
  // console.log(options);
  var result;
  var obStr = JSON.stringify(options);
  var a = obStr.replace(new RegExp(':', 'g'), '=');
  var b = a.replace(new RegExp(',', 'g'), '&');
  var c = b.replace(new RegExp('"', 'g'), '');
  result = c;
// RESULT = {"buy_currency":"EUR","sell_currency":"GBP","amount":10000,"fixed_side":"buy"}



  console.log('RESULT = ' + result);
}

// GET A QUOTE
function getQuote(token, options) {
  dataString(options);
  curl.request({
    method : 'GET',
    url : 'https://devapi.thecurrencycloud.com/v2/rates/detailed',
    headers : { 'X-Auth-Token' : token },
    data :  'buy_currency=' + options.buy_currency +
            '&sell_currency=' + options.sell_currency +
            '&amount=' + options.amount +
            '&fixed_side=' + options.fixed_side
    // data : 'buy_currency=EUR&sell_currency=GBP&amount=10000.00&fixed_side=buy'
  },
  function (err, stdout) {
    if (err) {console.log(err);}
    if (stdout) {console.log(stdout);}
  });
}


// e.g. options obj to pass in to convertion
// for demo purposes
var convertOptions = {
 buy_currency : 'EUR',
 sell_currency : 'GBP',
 amount : 10000,
 fixed_side : 'buy',
 reason : 'Invoice Payment',
 term_agreement : true
};


// CONVERT
function convert(token, convertOptions) {
  curl.request({
    method : 'POST',
    url : 'https://devapi.thecurrencycloud.com/v2/conversions/create',
    headers : { 'X-Auth-Token' : token },
    data :  'buy_currency=' + convertOptions.buy_currency +
            '&sell_currency=' + convertOptions.sell_currency +
            '&amount=' + convertOptions.amount +
            '&fixed_side=' + convertOptions.fixed_side +
            '&reason=' + convertOptions.reason +
            '&term_agreement=' + convertOptions.term_agreement
  },
  function (err, stdout) {
    if (err) {console.log(err);}
    if (stdout) {console.log(stdout);}
  });
}


var addBenOptions = {
  currency : 'ZAR',
  bank_account_country : 'ZA',
  account_number : '123456783',
  bic_swift : 'COBADEFF',
  // the following are for the createBeneficiary()
  bank_account_holder_name : 'Tasty Biltong',
  beneficiary_country : 'ZA',
  bank_country : 'ZA',
  iban : 'DE89370400440532013000'
  /*currency : 'EUR',
  bank_account_country : 'DE'*/
}
/* Requirements for ZA are
{ payment_type: 'priority',
  acct_number: '^\\d{1,16}$',
  bic_swift: '^[0-9A-Z]{8}$|^[0-9A-Z]{11}$' }
*/

// Add a Beneficiary
//
// We want to make a priority payment to a supplier based in Germany - change to south africa.
// To do this, we first need to check which details are required.

function addBeneficiary (token, addBenOptions) {
  var reqs;

  checkRequired();
  createBeneficiary();

  function checkRequired() {

    curl.request({
      method : 'GET',
      url : 'https://devapi.thecurrencycloud.com/v2/reference/beneficiary_required_details',
      headers : { 'X-Auth-Token' : token },
      data :  'currency=' + addBenOptions.currency +
              '&bank_account_country=' + addBenOptions.bank_account_country

    },
    function(err, stdout) {
      if (err) {console.log(err);}
      if (stdout) {
        reqs = JSON.parse(stdout).details[0];
        console.log(reqs);
      }
    });
  } // checkReqDets


  function createBeneficiary () {
    curl.request({
      method : 'POST',
      url : 'https://devapi.thecurrencycloud.com/v2/beneficiaries/create',
      headers : { 'X-Auth-Token' : token },
      data :  'currency=' + addBenOptions.currency +
              '&bank_account_holder_name=' + addBenOptions.bank_account_holder_name +
              '&beneficiary_country='  + addBenOptions.beneficiary_country +
              '&bank_country=' + addBenOptions.bank_country +
              '&account_number=' + addBenOptions.account_number +
              '&bic_swift=' + addBenOptions.bic_swift
    },
    function (err, stdout) {
      if (err) {console.log(err);}
      if (stdout) {console.log(stdout);}
    });
  }

} // addBeneficiary


var payOptions = {
  currency : 'ZAR',
  conversion_id : '11441095-87a4-4905-a3a0-cdbe312fc02e', // WHERE?
  beneficiary_id : 'ce267c2d-22cd-44b0-8cb0-43d069607e38',
  amount : 10000,
  reason : 'Inv:oice Payment',
  payment_type : 'priority',
  reference : 'Invoice 1234'
};

// PAY

function pay(token, payOptions) {
  curl.request({
    method : 'POST',
    url : 'https://devapi.thecurrencycloud.com/v2/payments/create',
    headers : { 'X-Auth-Token' : token },
    data :  'currency=' + payOptions.currency +
            '&conversion_id=' + payOptions.conversion_id +
            '&beneficiary_id=' + payOptions.beneficiary_id +
            '&amount=' + payOptions.amount +
            '&reason=' + payOptions.reason +
            '&payment_type=' + payOptions.payment_type +
            '&reference=' + payOptions.reference
  },
  function (err, stdout) {
    if (err) {console.log(err);}
    if (stdout) {console.log(stdout);}
  });
}




/*
curl -X POST \
--header "X-Auth-Token: ea6d13c7bc50feb46cf978d137bc01a2" \
-d "currency=EUR" \
-d "conversion_id=11441095-87a4-4905-a3a0-cdbe312fc02e" \
-d "beneficiary_id=ce267c2d-22cd-44b0-8cb0-43d069607e38" \
-d "amount=10000" \
-d "reason=Invoice Payment" \
-d "payment_type=priority" \
-d "reference=Invoice 1234" \
https://devapi.thecurrencycloud.com/v2/payments/create
*/







// TESTING
getAuthToken()
  .then(function (token) {
    // console.log(token); // DONE
    // getQuote(token, quoteOptions);
    convert(token, convertOptions);
    // addBeneficiary(token, addBenOptions);
    // pay(token, payOptions);
    // closeSession(token);
  });





// CLOSE SESSION
// close the session when the user logs out
// note it will expire anyway if we dont
function closeSession (token) {
  curl.request({
    method : 'POST',
    url : 'https://devapi.thecurrencycloud.com/v2/authenticate/close_session',
    headers : {'X-Auth-Token' : token}
  },
  function(err, stdout) {
    if(err) {
      console.log(err + err.stack);
    }
    if(stdout) {
      console.log(stdout);
    }
  });
}