'use strict';

var should = require('should'),
  request = require('supertest'),
  app = require('../../server'),
  agent = request.agent(app);


/**
 * Globals
 */
var user, article;

describe('currency cloud api tests', function () {

  xit('should be able to login', function (done) {
    agent
      .get('/currencycloud/login')
      .end(function (err, res) {
        (res.status).should.equal(200);
        agent
          .get('/currencycloud/closesession')
          .end(function (err, res) {
            (res.status).should.match(200);
          });
        done();
      });
  });

  xit('should be able to get a quote', function (done) {

    var quoteOptions = {
      buy_currency : 'EUR',
      sell_currency : 'GBP',
      amount : 10000,
      fixed_side : 'buy'
    };
    var authToken;

    agent
      .get('/currencycloud/login')
      .end(function (err, res) {
        (res.status).should.equal(200);
        agent
          .get('/currencycloud/getquote')
          .send(quoteOptions)
          .end(function (err, res) {
            (res.body.currency_pair).should.match('EURGBP');
          });


        done();
      });
  });

  it('should convert currency', function(done){
    // e.g. options obj to pass in to convertion
    // for demo purposes
    var convertOptions = {
     buy_currency : 'EUR',
     sell_currency : 'GBP',
     amount : 10000,
     fixed_side : 'buy',
     reason : 'Invoice Payment',
     term_agreement : true
    };

    agent
      .get('/currencycloud/login')
      .end(function (err, res) {
        (res.status).should.equal(200);
        agent
          .get('/currencycloud/convert')
          // .send(convertOptions)
          .end(function (err, res) {
            // (res.body.settlement_date).should.be.ok;
            if (err) {console.log(err);}
            if (res) {console.log(res); }
            agent
              .get('/currencycloud/closesession')
              .end(function (err, res) {
                (res.status).should.match(200);
              });
          });

        done();
      });
  });





});