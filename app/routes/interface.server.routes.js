'use strict';

module.exports = function(app) {
  var currencyApi = require('../../app/controllers/currencyapi');

  app.route('/currencycloud/login')
    .get(currencyApi.getAuthToken);
  app.route('/currencycloud/getquote')
    .get(currencyApi.getQuote);
  app.route('/currencycloud/convert')
    .get(currencyApi.convert);
  app.route('/currencycloud/closesession')
    .get(currencyApi.closeSession);
  app.route('/currencycloud/addbeneficiary')
    .get(currencyApi.addBeneficiary);
/*

  app.route('/currencycloud/pay')
    .get(currencyApi.pay);
*/
};